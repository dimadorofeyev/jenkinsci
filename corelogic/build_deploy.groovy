#!/usr/bin/env groovy

credentialsId = ['SA'  : '5a9b1abf-fd75-4084-bc2b-6b6cdc15b680',
                 'CI'  : '5a9b1abf-fd75-4084-bc2b-6b6cdc15b680',
                 'SB'  : '5a9b1abf-fd75-4084-bc2b-6b6cdc15b680',
                 'PERF': '5a9b1abf-fd75-4084-bc2b-6b6cdc15b680',
                 'INT' : '5a9b1abf-fd75-4084-bc2b-6b6cdc15b680',
                 'STG' : '5a9b1abf-fd75-4084-bc2b-6b6cdc15b680']

properties([[$class  : 'BuildDiscarderProperty',
        strategy: [$class: 'LogRotator', numToKeepStr: '10', artifactDaysToKeepStr: '14', artifactNumToKeepStr: '5']], disableConcurrentBuilds(), pipelineTriggers([[$class: 'GitHubPushTrigger']])])

    node('linux') {
      try {
        notifySlack("STARTED")

        env.PATH = "/usr/local/bin:" + env.PATH
        env.SPRING_PROFILES_ACTIVE = "ci"
        env.JAVA_HOME = tool 'Java8'

        stage 'Clean Workspace'
            deleteDir()

        stage 'Checkout'
            checkout scm

            def appVersion = appVersion().trim()
            def appName = appName().trim()
            def artifactName = appName + "-" + appVersion + ".jar"

            echo "ci App name: ${appName}"
            echo "ci Artifact Name: ${artifactName}"

        stage 'Building'
            sh 'chmod +x gradlew'
            sh './gradlew clean build -x test -x check --console=plain --no-daemon --stacktrace --rerun-tasks --refresh-dependencies'

        stage 'UnitTest Running'
                sh './gradlew test'
                stash includes: 'build/**/*.jar', name: 'build-artifacts'
                stash includes: 'build/resources/manifest*.yml', name: 'cf-configs'
                //stash includes: 'integration_spec/', name: 'integration-specs'

                // archive 'build/libs/*.jar'
                // archive 'build/resources/manifest.*.yml'
                // archive '*/set-env.sh'
                sh "ls -l build/libs/"
                sh "ls -l build/resources/"

        stage 'Codestyle Tests'
        parallel 'CheckStyle Running': {
                    sh './gradlew checkStyleMain'
                    sh './gradlew checkStyleTest'
              }, 'PmdTest Running': {
                    sh './gradlew pmdMain'
                    sh './gradlew pmdTest'
              }, 'FindbugsTest Running':{
                    sh './gradlew findbugsMain'
                    sh './gradlew findbugsTest'
    //          }, 'Sonar Analyses': {
    //              sh './gradlew sonarqube -PignoreFailedTests=true'
              }
        stage 'Publish Test Results'
               step([$class: 'JUnitResultArchiver', testResults: 'build/test-results/**/*.xml', allowEmptyResults: true])

       stage'Uploading to Nexus'
               sh "./gradlew clean assemble -x test uploadArchives -Pjenkins_build_number=${env.BUILD_NUMBER} --rerun-tasks "

       stage 'Undeploy SB'
               undeployFromEnv('SB', appName)

       deployToEnv('SB', artifactName, appName)

       notifySlack("SUCCESS")

      } catch (Exception e) {
          notifySlack("FAILED")
          throw e
      }
}

def deployToEnv(env, artifactName, appName = '') {
    stage "Deploy $env"

        echo "deploying ${appName} on ${env}"
        sh "ls -l build/libs/"

        def commandBuilder = "cf push ${appName} "
        commandBuilder += "-f build/resources/manifest-${env.toLowerCase()}.yml "
        commandBuilder += "-p build/libs/$artifactName "

        /*When an appName is passed in then reanme the host. This is use to have unique appname and routes
         to support concurrent integration testing in CI*/
        if (appName?.trim()) {
            commandBuilder += "-n ${appName}"
        }

        executeCFCommand(env, commandBuilder)
}

def undeployFromEnv(env, appName) {
    echo "undeploying ${appName} from ${env}"

    executeCFCommand(env, "cf delete -f -r ${appName}")
}

def notifyPulse(phase, status) {
    node('linux') {
        sh "curl -X POST -d ' { \"build\": { \"full_url\": \"\", \"number\": ${env.BUILD_NUMBER}, \"phase\": \"${phase}\", \"status\": \"${status}\" } }\' \"${pulseUrl}\""
    }
}

def executeCFCommand(env, command) {
    node('linux') {
        deleteDir()
        unstash 'build-artifacts'
        unstash 'cf-configs'

        wrap([$class                : 'CloudFoundryCliBuildWrapper',
              apiEndpoint           : 'https://api.preprodapp.cf.corelogic.net',
              cloudFoundryCliVersion: 'CloudFoundryCLI',
              credentialsId         : credentialsId[env],
              organization          : 'Corelogic',
              space                 : env]) {

            sh command;
        }
    }
}

def appVersion() {
    if (fileExists ('gradle.properties')) {
        def matcher = readFile('gradle.properties') =~ 'version=(.*)'
        matcher ? matcher[0][1] : null
    } else {
        return "There is no gradle.properties found"
    }
}

def appName() {
    if (fileExists ('settings.gradle')) {
        def matcher = readFile('settings.gradle') =~ 'rootProject.name=\"(.*)\"'
        matcher ? matcher[0][1] : null
    } else {
        return "There is no settings.gradle found"
    }
}

def appDomain(env) {
    if (fileExists ('manifests/manifest-${env.toLowerCase()}.yml')) {
        def matcher = readFile('manifests/manifest-${env.toLowerCase()}.yml') =~ 'domain:(.*)'
        matcher ? matcher[0][1] : null
    } else {
        return "There is no manifests/manifest-${env.toLowerCase()}.yml found"
    }
}

def notifySlack(text) {
    def slackURL = 'https://hooks.slack.com/services/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'
    if (text == "STARTED"){
        text = "\"STARTED: Job ${env.JOB_NAME} [${env.BUILD_NUMBER}] ${env.BUILD_URL}\""
        color = "#FFFF00"
    }
    if (text == "SUCCESS") {
        text = "\"SUCCESSFUL: Job ${env.JOB_NAME} [${env.BUILD_NUMBER}] ${env.BUILD_URL}\""
        color = "#00FF00"
    }
    if (text == "FAILED") {
        text = "\"FAILED: Job ${env.JOB_NAME} [${env.BUILD_NUMBER}] ${env.BUILD_URL}\""
        color = "#FF0000"
    }
    def payload = "{\"text\": ${text}, \"color\": \"${color}\"}"

    sh "curl -X POST --data-urlencode \'payload={\"attachments\": [${payload}]}' ${slackURL}"
}
